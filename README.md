# Testing-files

Files for learners from Katy Huff's SWC lesson on testing.
Lesson: http://katyhuff.github.io/python-testing/
Lesson repo: https://github.com/katyhuff/python-testing

This repo uses Travis-CI to run the tests on commit

